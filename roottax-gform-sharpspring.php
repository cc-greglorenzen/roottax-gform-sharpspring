<?php
/**
 *Plugin Name: Roottax Gform SharpSpring
 *Description: Integrating gravity forms on Roottax with SharpSpring
 *Author: Greg Lorenzen
 *Version: 1.0.2
**/
    // post_to_third_party_1 = subscribe to blog form

    add_action( 'gform_after_submission_1', 'post_to_third_party_1', 10, 2 );

    function post_to_third_party_1( $entry, $form ) {
        $baseURI = 'https://app-3QNFDR541O.marketingautomation.services/webforms/receivePostback/MzawMDGzNDaxAAA/';
        $endpoint = '1de866e6-5166-4471-b226-74e45d832022';
        $post_url = $baseURI . $endpoint;

        $body = array(
            'Email' => rgar( $entry, '1' ),
            'trackingid__sb' => $_COOKIE['__ss_tk']
        );
        $request = new WP_Http();

        $response = $request->post( $post_url, array( 'body' => $body ) );
    }
?> 